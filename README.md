# Flame SpaceFleet

Cross-platform game made with Flame Engine for Flutter.

## Screenshots

<img src="./screenshots/home.png" alt="Home screenshot" width="240" /> <img src="./screenshots/game.png" alt="Game screenshot" width="240" /> <img src="./screenshots/gameover.png" alt="Game over screenshot" width="240" />

## Features

- Keyboard and touch controls for multiple device types.
- Animated sprites for idle actions.
- Flipping player image to reduce required images.
- Dynamic size and color for health bar.

## Releases

See this project working on web at this link:

[WebApp](https://cbau.gitlab.io/flame_spacefleet/)

## Compile

To run this project on your device, run this command:

```bash
flutter run --release
```
