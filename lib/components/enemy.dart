import 'dart:math';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flame/sprite.dart';
import 'package:flame_spacefleet/components/bullet.dart';
import 'package:flame_spacefleet/games/space_fleet_game.dart';
import 'package:flutter/material.dart';

class Enemy extends SpriteAnimationComponent
    with CollisionCallbacks, HasGameRef<SpaceFleetGame> {
  final _animationSpeed = 0.15;
  final _size = Vector2(80, 80);
  final _speed = 160;

  var _health = 3;

  double get _randomX => 240.0 - 80.0 * Random().nextInt(7);

  @override
  void onCollisionStart(
      Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollisionStart(intersectionPoints, other);

    if (other is Bullet) {
      _health--;
      other.removeFromParent();

      if (_health > 0) {
        add(ColorEffect(
          Colors.white,
          const Offset(1, 0),
          EffectController(duration: 0.5),
        ));
      } else {
        gameRef.add(Enemy());
        gameRef.addScore(1);
        _reset();
      }
    }
  }

  @override
  Future<void>? onLoad() async {
    super.onLoad();

    await _loadAnimations();

    anchor = Anchor.center;
    size = _size;

    if (position == Vector2.zero()) {
      position = Vector2(_randomX, -580);
    }

    //paint.colorFilter = const ColorFilter.mode(Color(0xFFFF0000), BlendMode.modulate);

    add(CircleHitbox());
  }

  @override
  void update(double dt) {
    super.update(dt);

    _move(dt);
  }

  Future<void> _loadAnimations() async {
    final spriteSheet = SpriteSheet(
      image: await gameRef.images.load('enemy.png'),
      srcSize: _size,
    );

    animation = spriteSheet.createAnimation(
      row: 0,
      stepTime: _animationSpeed,
      to: 3,
    );
  }

  void _move(double dt) {
    // Component movement.
    position.add(Vector2(0, dt * _speed));

    // Out of world.
    if (position.y > 580) {
      _reset();
    }
  }

  void _reset() {
    _health = 2;
    position = Vector2(_randomX, -580);
  }
}
