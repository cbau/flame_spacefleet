import 'package:flame/components.dart';
import 'package:flutter/material.dart';

class ProgressBar extends NineTileBoxComponent {
  late final double _centerEdge;
  late final NineTileBox _contentTileBox;

  final _contentPaint = Paint();

  double progress;

  ProgressBar({
    this.progress = 1,
  });

  set barColor(Color value) =>
      _contentPaint.colorFilter = ColorFilter.mode(value, BlendMode.modulate);

  @override
  Future<void>? onLoad() async {
    super.onLoad();

    nineTileBox = NineTileBox(await Sprite.load('bar-border.png'));
    _contentTileBox = NineTileBox(await Sprite.load('bar-content.png'));
    _centerEdge = (_contentTileBox.sprite.src.width ~/ 3).toDouble();
  }

  @override
  void render(Canvas c) {
    c.drawImageNine(
      _contentTileBox.sprite.image,
      Rect.fromLTWH(_centerEdge, _centerEdge, _centerEdge, _centerEdge),
      Rect.fromLTWH(0, 0, width * progress, height),
      _contentPaint,
    );
    super.render(c);
  }
}
