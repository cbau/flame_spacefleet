import 'package:flame/components.dart';
import 'package:flame/parallax.dart';
import 'package:flutter/widgets.dart';

class Background extends ParallaxComponent {
  @override
  Future<void>? onLoad() async {
    super.onLoad();

    parallax = await gameRef.loadParallax(
      [
        ParallaxImageData('nebula.jpg'),
      ],
      baseVelocity: Vector2(0, -10),
      fill: LayerFill.width,
      repeat: ImageRepeat.repeatY,
    );
  }
}
