import 'dart:math';
import 'dart:ui';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flame/particles.dart';
import 'package:flame/sprite.dart';
import 'package:flame_spacefleet/components/bullet.dart';
import 'package:flame_spacefleet/components/enemy.dart';
import 'package:flame_spacefleet/components/progress_bar.dart';
import 'package:flame_spacefleet/extensions.dart';
import 'package:flame_spacefleet/overlays/game_over_overlay.dart';
import 'package:flutter/services.dart';

class Player extends SpriteAnimationComponent
    with CollisionCallbacks, HasGameRef, KeyboardHandler {
  var isMovingLeft = false;
  var isMovingRight = false;
  var isShooting = false;

  final ProgressBar? healthBar;

  late final SpriteAnimation _idleAnimation;
  late final SpriteAnimation _movingAnimation;

  final _animationSpeed = 0.15;
  final _dangerColor = const Color(0xFFCC0000);
  final _shootSpeed = 0.15;
  final _size = Vector2(80, 80);
  final _speed = 320;
  final _warningColor = const Color(0xFFFFCC00);

  var _health = 5;
  var _inmunityTimer = 0.0;
  var _shootTimer = 0.0;

  Player({this.healthBar});

  @override
  void onCollisionStart(
      Set<Vector2> intersectionPoints, PositionComponent other) {
    super.onCollisionStart(intersectionPoints, other);

    if (_inmunityTimer <= 0 && other is Enemy) {
      _addDamage(1);
    }
  }

  @override
  bool onKeyEvent(RawKeyEvent event, Set<LogicalKeyboardKey> keysPressed) {
    isMovingLeft = keysPressed.contains(LogicalKeyboardKey.arrowLeft);
    isMovingRight = keysPressed.contains(LogicalKeyboardKey.arrowRight);
    isShooting = keysPressed.contains(LogicalKeyboardKey.space);

    return super.onKeyEvent(event, keysPressed);
  }

  @override
  Future<void>? onLoad() async {
    super.onLoad();

    await _loadAnimations();

    anchor = Anchor.center;
    animation = _idleAnimation;
    position = Vector2(0, 440);
    size = _size;

    add(CircleHitbox());
  }

  @override
  void update(double dt) {
    super.update(dt);

    _move(dt);
    _shoot(dt);

    if (_inmunityTimer > 0) {
      _inmunityTimer -= dt;
    }
  }

  void _addDamage(int quantity) {
    _inmunityTimer = 1;
    _health -= quantity;
    healthBar?.progress = _health * 0.2;
    if (_health > 0) {
      if (_health < 3) {
        healthBar?.barColor = _health == 2 ? _warningColor : _dangerColor;
      }
      add(OpacityEffect.fadeOut(
        EffectController(
          duration: 0.25,
          reverseDuration: 0.25,
          repeatCount: 2,
        ),
      ));
    } else {
      _die();
    }
  }

  void _die() {
    final random = Random();
    final particle = CircleParticle(
      paint: Paint()..color = const Color(0xFFFFFFFF),
      radius: 1.0,
    );
    gameRef.add(ParticleSystemComponent(
      particle: Particle.generate(
        count: 800,
        generator: (i) => MovingParticle(
          from: position,
          lifespan: 10,
          to: position +
              (Vector2.zero()..setFromRadians(random.nextDouble() * pi * 2)) *
                  (random.nextDouble() * 90),
          child: particle,
        ),
      ),
    ));
    gameRef.overlays.add(GameOverOverlay.name);
    removeFromParent();
  }

  Future<void> _loadAnimations() async {
    final spriteSheet = SpriteSheet(
      image: await gameRef.images.load('ship.png'),
      srcSize: _size,
    );

    _idleAnimation = spriteSheet.createAnimation(
      row: 0,
      stepTime: _animationSpeed,
      to: 3,
    );

    _movingAnimation = spriteSheet.createAnimation(
      row: 1,
      stepTime: _animationSpeed,
      to: 3,
    );
  }

  void _move(double dt) {
    // Component movement.
    final vector = Vector2.zero();

    if (isMovingLeft) {
      vector.x -= dt * _speed;
    }

    if (isMovingRight) {
      vector.x += dt * _speed;
    }

    position.add(vector);

    // Component animation.
    if (vector.x == 0) {
      animation = _idleAnimation;
    } else {
      animation = _movingAnimation;
      scale.x = vector.x < 0 ? -1 : 1;
    }

    // Contain in world.
    if (position.x > 240) {
      position.x = 240;
    } else if (position.x < -240) {
      position.x = -240;
    }
  }

  void _shoot(double dt) {
    if (_shootTimer > 0) {
      _shootTimer -= dt;
    } else if (isShooting) {
      _shootTimer = _shootSpeed;
      gameRef.add(Bullet()..position = Vector2(x, y - 60));
    }
  }
}
