import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/sprite.dart';

class Bullet extends SpriteAnimationComponent with HasGameRef {
  final _animationSpeed = 0.15;
  final _size = Vector2(40, 40);
  final _speed = 640;

  @override
  Future<void>? onLoad() async {
    super.onLoad();

    await _loadAnimations();

    anchor = Anchor.center;
    size = _size;

    add(CircleHitbox());
  }

  @override
  void update(double dt) {
    super.update(dt);

    _move(dt);
  }

  Future<void> _loadAnimations() async {
    final spriteSheet = SpriteSheet(
      image: await gameRef.images.load('bullet.png'),
      srcSize: _size,
    );

    animation = spriteSheet.createAnimation(
      row: 0,
      stepTime: _animationSpeed,
      to: 2,
    );
  }

  void _move(double dt) {
    // Component movement.
    position.add(Vector2(0, -dt * _speed));

    // Out of world.
    if (position.y < -560) {
      removeFromParent();
    }
  }
}
