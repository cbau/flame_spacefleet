import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flame/sprite.dart';
import 'package:flame_spacefleet/components/background.dart';
import 'package:flame_spacefleet/components/enemy.dart';
import 'package:flame_spacefleet/components/player.dart';
import 'package:flame_spacefleet/components/progress_bar.dart';
import 'package:flame_spacefleet/overlays/pause_overlay.dart';
import 'package:flutter/material.dart';

class SpaceFleetGame extends FlameGame
    with HasCollisionDetection, HasKeyboardHandlerComponents, HasTappables {
  Player? _player;
  var _score = 0;
  final _scoreText = TextComponent(text: 'Score: 000')
    ..position = Vector2(-270, -530)
    ..priority = 1;

  @override
  Future<void>? onLoad() async {
    super.onLoad();

    camera.setRelativeOffset(Anchor.center);

    final controlsSheet = SpriteSheet.fromColumnsAndRows(
      image: await images.load('controls.png'),
      columns: 4,
      rows: 1,
    );
    final healthBar = ProgressBar()
      ..barColor = const Color(0xFF00CC00)
      ..position = Vector2(150, -530)
      ..priority = 1
      ..size = Vector2(120, 20);
    _player = Player(healthBar: healthBar);
    final tileBox = NineTileBox(await Sprite.load('border.png'));

    add(Background());
    add(NineTileBoxComponent(
      nineTileBox: tileBox,
      position: Vector2(-280, -540),
      scale: Vector2(-1, 1),
      size: Vector2(80, 1080),
    ));
    add(NineTileBoxComponent(
      nineTileBox: tileBox,
      position: Vector2(280, -540),
      size: Vector2(80, 1080),
    ));
    add(_player!);
    add(Enemy()..position = Vector2(-80, -580));
    add(Enemy()..position = Vector2(80, -740));
    add(Enemy()..position = Vector2(-160, -1080));
    add(Enemy()..position = Vector2(160, -1160));
    add(healthBar);
    add(_scoreText);
    add(HudButtonComponent(
      button: SpriteComponent(
        sprite: controlsSheet.getSpriteById(3),
      ),
      onReleased: () => setPause(true),
      position: Vector2(camera.viewport.effectiveSize.x / 2 - 20, 40),
    ));
    add(HudButtonComponent(
      button: SpriteComponent(
        sprite: controlsSheet.getSpriteById(0),
      ),
      margin: const EdgeInsets.only(
        bottom: 40,
        left: 40,
      ),
      onPressed: () => _player?.isMovingLeft = true,
      onReleased: () => _player?.isMovingLeft = false,
    ));
    add(HudButtonComponent(
      button: SpriteComponent(
        sprite: controlsSheet.getSpriteById(1),
      ),
      margin: const EdgeInsets.only(
        bottom: 40,
        left: 80,
      ),
      onPressed: () => _player?.isMovingRight = true,
      onReleased: () => _player?.isMovingRight = false,
    ));
    add(HudButtonComponent(
      button: SpriteComponent(
        sprite: controlsSheet.getSpriteById(2),
      ),
      margin: const EdgeInsets.only(
        bottom: 40,
        right: 40,
      ),
      onPressed: () => _player?.isShooting = true,
      onReleased: () => _player?.isShooting = false,
    ));
  }

  @override
  void lifecycleStateChange(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      setPause(true);
    }

    super.lifecycleStateChange(state);
  }

  @override
  void onGameResize(Vector2 canvasSize) {
    super.onGameResize(canvasSize);
    _resizeCamera();
  }

  void addScore(int value) {
    _score += value;
    _scoreText.text = 'Score: ${_score}00';
  }

  void setPause(bool value) {
    if (value) {
      overlays.add(PauseOverlay.name);
      pauseEngine();
    } else {
      overlays.remove(PauseOverlay.name);
      resumeEngine();
    }
  }

  void _resizeCamera() {
    camera.zoom = canvasSize.y / 1080;
  }
}
