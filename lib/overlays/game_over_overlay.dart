import 'package:flutter/material.dart';

import '../screens/game_screen.dart';

class GameOverOverlay extends StatelessWidget {
  static const name = 'gameOverOverlay';

  const GameOverOverlay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Game Over',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  OutlinedButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: const Text('Exit'),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  OutlinedButton(
                    autofocus: true,
                    onPressed: () => Navigator.of(context)
                        .pushReplacementNamed(GameScreen.route),
                    child: const Text('Retry'),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
