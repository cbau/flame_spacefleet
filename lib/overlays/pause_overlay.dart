import 'package:flame_spacefleet/games/space_fleet_game.dart';
import 'package:flutter/material.dart';

class PauseOverlay extends StatelessWidget {
  static const name = 'pauseOverlay';

  const PauseOverlay(this.gameRef, {Key? key}) : super(key: key);

  final SpaceFleetGame gameRef;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Pause',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  OutlinedButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: const Text('Exit'),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  OutlinedButton(
                    autofocus: true,
                    onPressed: () => gameRef.setPause(false),
                    child: const Text('Resume'),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
