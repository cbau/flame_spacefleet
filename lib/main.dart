import 'package:flame_spacefleet/screens/game_screen.dart';
import 'package:flame_spacefleet/screens/main_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      routes: {
        GameScreen.route: (context) => GameScreen(),
        MainScreen.route: (context) => const MainScreen(),
      },
      theme: ThemeData(
        brightness: Brightness.dark,
        fontFamily: 'Bungee',
        scaffoldBackgroundColor: const Color(0xFF000000),
      ),
    ),
  );
}
