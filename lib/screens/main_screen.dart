import 'package:flame_spacefleet/screens/game_screen.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  static const route = '/';

  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Space\nFleet',
              style: Theme.of(context).textTheme.displayMedium?.copyWith(
                    fontStyle: FontStyle.italic,
                    height: 1,
                  ),
            ),
            const SizedBox(
              height: 32,
            ),
            OutlinedButton(
              autofocus: true,
              onPressed: () =>
                  Navigator.of(context).pushNamed(GameScreen.route),
              child: const Text('Start'),
            ),
            const SizedBox(
              height: 32,
            ),
            const Text('Instructions:'),
            const Text('< > : Move'),
            const Text('[SPACE] : Shoot'),
            const Text('[ENTER] : Pause'),
          ],
        ),
      ),
    );
  }
}
