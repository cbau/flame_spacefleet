import 'package:flame/game.dart';
import 'package:flame_spacefleet/overlays/game_over_overlay.dart';
import 'package:flame_spacefleet/overlays/pause_overlay.dart';
import 'package:flutter/material.dart';

import '../games/space_fleet_game.dart';

class GameScreen extends StatelessWidget {
  static const route = '/game';

  final _game = SpaceFleetGame();

  GameScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        _game.setPause(true);
        return false;
      },
      child: GameWidget(
        game: _game,
        overlayBuilderMap: <String,
            Widget Function(BuildContext, SpaceFleetGame)>{
          GameOverOverlay.name: (context, game) => const GameOverOverlay(),
          PauseOverlay.name: (context, game) => PauseOverlay(game),
        },
      ),
    );
  }
}
