import 'dart:math';

import 'package:flame/components.dart';

extension Vector2Extension on Vector2 {
  void setFromRadians(double radians) {
    x = cos(radians);
    y = sin(radians);
  }
}
